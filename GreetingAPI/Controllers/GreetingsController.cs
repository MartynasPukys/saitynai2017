﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreetingAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GreetingAPI.Controllers
{
    [Route("api/[controller]")]
    public class GreetingsController : Controller
    {
        private readonly GreetingContext _context;

        public GreetingsController(GreetingContext context)
        {
            _context = context;

            if(_context.Greetings.Count() == 0)
            {
                _context.Greetings.Add(new Greeting { GreetingText = "ka jus" });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Greeting> GetAll()
        {
            return _context.Greetings.ToList();
        }

        [Authorize]
        [HttpGet("{id}", Name = "GetGreeting")]
        public IActionResult GetById(long id)
        {
            var item = _context.Greetings.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/<Greetings>
        [HttpPost]
        public IActionResult Create([FromBody]Greeting greeting)
        {
            if(greeting == null)
            {
                return BadRequest();
            }

            _context.Greetings.Add(greeting);
            _context.SaveChanges();

            return Ok();

        }

        // PUT api/<Greetings>/5
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody]Greeting greeting)
        {
            if (greeting == null)
            {
                return BadRequest();
            }

            Greeting greetingToUpdate = _context.Greetings.FirstOrDefault(t => t.Id == id);
            if (greetingToUpdate == null)
                return BadRequest();

            greetingToUpdate.CelebratorName = greeting.CelebratorName;
            greetingToUpdate.CreatorName = greeting.CreatorName;
            greetingToUpdate.DateCreated = greeting.DateCreated;
            greetingToUpdate.GreetingText = greeting.GreetingText;
      
            _context.Greetings.Update(greetingToUpdate);
            _context.SaveChanges();

            return Ok();
        }

        // DELETE api/<Greetings>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Greeting greetingToRemove = _context.Greetings.FirstOrDefault(t => t.Id == id);
            if (greetingToRemove == null)
                return NotFound();

            _context.Greetings.Remove(greetingToRemove);
            _context.SaveChanges();

            return Ok();
        }
    }
}
