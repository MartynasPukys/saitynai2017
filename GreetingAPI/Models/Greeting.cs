﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreetingAPI.Models
{
    public class Greeting
    {
        public long Id { get; set; }
        public string GreetingText { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatorName { get; set; }
        public string CelebratorName { get; set; }
    }
}
